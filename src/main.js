import Vue from 'vue'
import Buefy from 'buefy'
import router from './router'
import store from './store/store'

// Vue Select
import vSelect from 'vue-select'
Vue.component('v-select', vSelect)
import 'vue-select/dist/vue-select.css';

import mixins from './mixins/mixins'

import axios from 'axios';

import App from './App.vue'

Vue.use(Buefy)

Vue.use(require('vue-moment'));


import Datetime from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.css'
Vue.use(Datetime)


// config file with base endpoint url if this file not found please copy env.js.dist from root folder
import { baseEndpointUrl } from './../env';

axios.defaults.baseURL = baseEndpointUrl;


const  accessToken  =  (localStorage.getItem('userToken')) ? 'Bearer ' + localStorage.getItem('userToken'): '';

if (accessToken) {
    axios.defaults.headers.common['Authorization'] = accessToken
}
  

// axios.interceptors.response.use((response) => {
//     // return response;
// }, function (error) {
//     // Do something with response error
//     // if (error.response.status === 400) {
//     //     localStorage.removeItem('userToken');
//     //     localStorage.removeItem('loggedInUseId');

//     //     // get return url from route parameters or default to '/'
//     //     return router.push('/login')
//     // }
//     // return Promise.reject(error.response);
// });




// Vue Mixins
Vue.mixin({
  mixins: [mixins]
})



// Global Components
Vue.component('loading-data', {
    data: function () {
      return {
        isLoading: true
      }
    },
    template: `
    <div class="loading--data">
          <b-loading :is-full-page="false" :active.sync="isLoading" :can-cancel="false">
              <b-icon
                  pack="fas"
                  icon="spinner"
                  size="is-large"
                  custom-class="fa-spin">
              </b-icon>
          </b-loading>
      </div>`
  })


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

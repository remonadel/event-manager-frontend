
const tasks = [
    {
        id: '1',
        title: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.  sit amet consectetur adipisicing elit',
        description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Lorem ipsum dolor, sit amet consectetur adipisicing elit. ',
        date: '1 month ago',
        groups: ['Editor','Admin', 'Designer'],
    },
    {
        id: '2',
        title: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
        description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Lorem ipsum dolor, sit amet consectetur adipisicing elit. ',
        date: '1 month ago',
        groups: ['Editor','Admin', 'Designer'],
    },
    {
        id: '3',
        title: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
        description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Lorem ipsum dolor, sit amet consectetur adipisicing elit. ',
        date: '1 month ago',
        groups: ['Editor','Admin', 'Designer'],
    },
    {
        id: '4',
        title: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
        description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Lorem ipsum dolor, sit amet consectetur adipisicing elit. ',
        date: '1 month ago',
        groups: ['Editor','Admin', 'Designer'],
    },
    {
        id: '5',
        title: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
        description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Lorem ipsum dolor, sit amet consectetur adipisicing elit. ',
        date: '1 month ago',
        groups: ['Editor','Admin', 'Designer'],
    },
    
]

export default tasks;
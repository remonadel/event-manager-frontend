
const groups = [
    {
        id: '1',
        name: 'groups name one',
        usersCount: 42,
    },
    {
        id: '2',
        name: 'groups name Two',
        usersCount: 42,
    },
    {
        id: '3',
        name: 'groups name Three',
        usersCount: 42,
    },
    {
        id: '4',
        name: 'groups name Four',
        usersCount: 42,
    },
    {
        id: '5',
        name: 'groups name Five',
        usersCount: 42,
    },
]

export default groups;
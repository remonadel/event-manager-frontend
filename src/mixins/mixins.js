import store from '../store/store'
import { mapState} from 'vuex';

const mixins = {
     store: store,
     data() {
         return {
             userRole: [],
         }
     },
     computed: {
          ...mapState({
               userData: state => state.login.userData,
          })
     },
     methods:{
          isInUserPermissions(moduleName) {
              if  (this.userRole && this.userRole === 'ROLE_ADMIN') {
                  return true;
              } else {
                  if  (moduleName === 'events') {
                      return true;
                  }
              }
              return false;
           },
     },
     watch: {
          userData: {
              handler() {
                  if (this.userData && this.userData.id) {
                    this.userRole = (this.userData.roles && this.userData.roles[0]) ? this.userData.roles[0] : []
                  }
              },
              immediate: true,
          },
      }
}

export default mixins;

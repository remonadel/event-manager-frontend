import axios from 'axios'
import router from './../../router'
const Register = {
     state: {
          isLoading: false,
          registerErrorMessage: null,
          registerSuccessful: false,
          userData: [],
        },
        mutations: {
          resetState(state){
            state.isLoading = true
            state.registerErrorMessage = null
            state.registerSuccessful = false
          },
        },
        actions: {
          register({ commit, state, dispatch }, data) {
            axios.post('/register', data)
              .then((response) => {
                state.isLoading = false
                state.registerSuccessful = true
                window.location.href = '/'
              }).catch(error => {
                state.registerErrorMessage = error.response.data.message
                state.isLoading = false
              })
          },
          checkUserData({ commit, state }, loginData) {
            axios.get('/me')
              .then((response) => {
                state.userData = response.data.data              
              })
              .catch(error => {
                // state.loginErrorMessage = error.data.data[0]
              })

          }
        },
     getters: {}
}

export default Register
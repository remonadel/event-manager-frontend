import axios from 'axios'
import router from './../../router'
const Login = {
     state: {
          isLoading: false,
          loginErrorMessage: null,
          loginSuccessful: false,
          userData: [],

        },
        mutations: {
          resetState(state){
            state.isLoading = true
            state.loginErrorMessage = null
            state.loginSuccessful = false
          },
          // Logout
          logout(){
            localStorage.removeItem('userToken');
            router.push('/login')
          },
          setUserData(state, playload) {
              state.userData = playload;
          },
        },
        actions: {
          doLogin({ commit, state, dispatch }, loginData) {
              commit('resetState');
              
            axios.post('/login_check', loginData)
              .then((response) => {
                state.isLoading = false
                localStorage.setItem('userToken', response.data.token)
                window.location.href = '/'
                
              }).catch(error => {
                state.loginErrorMessage = error.response.data.message
                state.isLoading = false
              })
          },
          checkUserData({ commit, state }, loginData) {
            axios.get('/me')
              .then((response) => {
                state.userData = response.data.data              
              })
              .catch(error => {
                // state.loginErrorMessage = error.data.data[0]
              })

          }
        },
     getters: {}
}

export default Login
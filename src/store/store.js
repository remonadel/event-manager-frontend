import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import login from './modules/Login'
import register from './modules/Register'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
    
  },
  actions: {

  },
  modules:{
    login, register
  }
})

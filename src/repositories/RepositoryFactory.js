import usersRepository from "./usersRepository";
import eventsRepository from "./eventsRepository";

const repositories = {
    users: usersRepository,
    events: eventsRepository,
};

export const RepositoryFactory = {
    get: name => repositories[name]
};

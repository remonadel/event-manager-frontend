import Repository from "./Repository";

const resource = "/events";

export default {
    getEvent(id) {
        return Repository.get(`${resource}/${id}`).then(response => {
            if (response.data.success) {
                return response.data.data;
            }
        });
    },
    updateEvent(id, data) {
        return Repository.put(`${resource}/${id}`, data).then(response => {
            if (response.data.success) {
                return response.data;
            }
        });
    },
    deleteEvent(id) {
        return Repository.delete(`${resource}/${id}`).then(response => {
            if (response.data.success) {
                return response.data;
            }
        });
    },
    getAllEvents(page, limit, filters = {}, isDone = null, isClosed = 0) {
        let searchQuery =
            filters.searchQuery && filters.searchQuery !== ""
                ? "&q=" + filters.searchQuery + "&fieldsNames=name"
                : "";

        let filterQuery = searchQuery;

        return Repository.get(
            `${resource}?page=${page}&limit=${limit}&isDone=${isDone}&order=desc&orderBy=id&pagination=1` +
                filterQuery
        ).then(response => {
            if (response.data.success) {
                return response.data.data;
            }
        });
    },
    getMyEvents(page, limit, filters = {}) {
        let searchQuery =
            filters.searchQuery && filters.searchQuery !== ""
                ? "&q=" + filters.searchQuery + "&fieldsNames=title"
                : "";

        let filterQuery = searchQuery;

        return Repository.get(
            `/my_events?page=${page}&limit=${limit}&order=desc&orderBy=id&pagination=1` +
                filterQuery
        ).then(response => {
            if (response.data.success) {
                return response.data.data;
            }
        });
    },
    newEvent(data) {
        return Repository.post(`${resource}`, data)
            .then(response => {
                if (response.data.success) {
                    return response.data;
                }
            })
            .catch(error => {
                return error.response.data;
            });
    },

    joinUser(data, eventId) {
        return Repository.post(`${resource}/${eventId}/users`, data)
            .then(response => {
                if (response.data.success) {
                    return response.data;
                }
            })
            .catch(error => {
                return error.response.data;
            });
    }
};

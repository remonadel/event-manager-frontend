import Repository from "./Repository";
import axios from "axios/index";

const resource = "/users";

export default {
  getUser(id) {
    return Repository.get(`${resource}/${id}`).then(response => {
      if (response.data.success) {
        return response.data.data;
      }
    });
  },
  getMyProfile() {
    return Repository.get("/me").then(response => {
      if (response.data.success) {
        return response.data.data;
      }
    });
  },
  updateUser(id, data) {
    return Repository.put(`${resource}/${id}`, data)
      .then(response => {
        if (response.data.success) {
          return response.data;
        }
      })
      .catch(error => {
        return error.response.data;
      });
  },
  deleteUser(id) {
    return Repository.delete(`${resource}/${id}`)
      .then(response => {
        if (response.data.success) {
          return response.data;
        }
      })
      .catch(error => {
        return error.response.data;
      });
  },
  getAllUsers(page, limit, filters = {}) {
    let searchQuery =
      filters.searchQuery && filters.searchQuery !== ""
        ? "&q=" + filters.searchQuery + "&fieldsNames=name,email"
        : "";
    let statusQuery =
      filters.status === 0 || filters.status === 1
        ? "&isActive=" + filters.status
        : "";

    let filterQuery = searchQuery + statusQuery;

    return Repository.get(
      `${resource}?page=${page}&limit=${limit}&order=desc&orderBy=id&pagination=1` +
        filterQuery
    ).then(response => {
      if (response.data.success) {
        return response.data.data;
      }
    });
  },
  newUser(data) {
    return Repository.post(`${resource}`, data)
      .then(response => {
        if (response.data.success) {
          return response.data;
        }
      })
      .catch(error => {
        return error.response.data;
      });
  }
};

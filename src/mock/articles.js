
const articles = [
    {
        id: '1',
        title: 'Article Title Test One',
        status: true,
    },
    {
        id: '2',
        title: 'Article Title Test Two',
        status: false,
    },
    {
        id: '3',
        title: 'Article Title Test Three',
        status: true,
    },
    {
        id: '4',
        title: 'Article Title Test Four',
        status: true,
    },
    {
        id: '5',
        title: 'Article Title Test Five',
        status: false,
    },
]

export default articles;
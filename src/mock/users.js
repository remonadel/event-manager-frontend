
const users = [
    {
        id: '1',
        title: 'user name one',
        email: 'test@email.test',
    },
    {
        id: '2',
        title: 'user name Two',
        email: 'test@email.test',
    },
    {
        id: '3',
        title: 'user name Three',
        email: 'test@email.test',
    },
    {
        id: '4',
        title: 'user name Four',
        email: 'test@email.test',
    },
    {
        id: '5',
        title: 'user name Five',
        email: 'test@email.test',
    },
]

export default users;
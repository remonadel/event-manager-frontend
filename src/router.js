import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/dashboard/Dashboard.vue'
import Wrapper from './Wrapper.vue'

Vue.use(Router)


import axios from "axios";

function userHasPermission(userRule, moduleName) {
    if  (userRule === 'ROLE_ADMIN') {
        return true;
    } else {
        if  (moduleName === 'events') {
            return true;
        }
    }
    return false;
}

import store from './store/store.js'

function routerGuard(to, from, next) {
    let userToken =  localStorage.getItem('userToken');
    if (userToken) {
        axios.get('/me')
        .then(response => {
            
            if (response.data.success) {
                let user = response.data.data;
                if (user) {
                    if (to.meta.moduleName) {
                        if (userHasPermission(user.roles[0], to.meta.moduleName)) {
                            return next();
                        }
                        return next('/not_authorized')
                    } else {
                        return next();
                    }
                }
            }
        }).catch(error => {
            window.location.href = '/logout'
        });
    }
}

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/login', name: 'login', component: () => import('./views/pages/Login') },
    { path: '/register', name: 'register', component: () => import('./views/pages/Register') },
    {path: '/logout', component: () => import('./views/pages/Logout')},
    {
      path: '/',
      component: Wrapper,
      children:[
        {
          path: '/',
          name: 'dashboard',
          component: Dashboard,
           beforeEnter: routerGuard,
          // redirect: '/',
        },
        {
          path: '/users',
          name: 'users',
          meta: {moduleName: 'users'},
           beforeEnter: routerGuard,
          component: () => import('./views/users/Users.vue')
        },
        {
          path: '/userForm/:id?',
          name: 'userForm',
          meta: {moduleName: 'users'},
           beforeEnter: routerGuard,
          component: () => import('./views/users/Form.vue')
        },
        {
          path: '/events',
          name: 'events',
          meta: {moduleName: 'events'},
           beforeEnter: routerGuard,
          component: () => import('./views/events/Events.vue')
        },
        {
          path: '/myEvents',
          name: 'myEvents',
          beforeEnter: routerGuard,
          component: () => import('./views/events/MyEvents.vue')
        },
        {
          path: '/eventForm/:id?',
          name: 'eventForm',
          meta: {moduleName: 'events'},
           beforeEnter: routerGuard,
          component: () => import('./views/events/Form.vue')
        },
        {
          path: '/addUserToEvent/:id?',
          name: 'joinUser',
          meta: {moduleName: 'events'},
          beforeEnter: routerGuard,
          component: () => import('./views/events/JoinUserForm.vue')
        },
        {
          path: '/not_authorized',
          name: 'notAuthorized',
          component: () => import('./views/pages/NotAuthorized'),
            beforeEnter: routerGuard
          },
      ]
    },
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})


// router.beforeEach((to, from, next) => {
// 	const token = localStorage.getItem('userToken');
// 	if (to.meta.requiresAuth) {
// 		if (token) {
// 			next()
// 		} else {
// 			next({ name: 'login' })
// 		}
// 	} else {
// 		next();
// 	}
// });	


router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login', '/register'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('userToken');
  if (to.path == '/login' && loggedIn) {
      router.push({ path: '/' })
  }
  if (authRequired && !loggedIn) {
      return next({
          path: '/login',
          query: { returnUrl: to.path }
      });
  }

  next();
})


export default router;

